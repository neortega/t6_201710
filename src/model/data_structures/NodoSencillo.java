package model.data_structures;

public class NodoSencillo <K,T> {
	
	private T valor;
	
	private K key;
	
	private NodoSencillo<K,T> siguiente;
	
	
	public NodoSencillo(K pKey, T pObjeto)
	{
		valor = pObjeto;
	}
	
	public T darObjeto()
	{
		return valor;
	}
	
	public NodoSencillo<K,T> darSiguiente()
	{
		return siguiente;
	}
	
	public K darLlave()
	{
		return key;
	}
	
	public void modificarSiguiente(K pKey, T pNuevoSiguiente)
	{
		siguiente =  new NodoSencillo<K,T>(pKey, pNuevoSiguiente);
	}
}
